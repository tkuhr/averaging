from setuptools import setup

__project__ = "averaging"
__version__ = "1.0.0"
__description__ = "a Python module for automated production of HFLAV averages"
__packages__ = ["averaging"]
__author__ = "Thomas Kuhr"
__author_email__ = "Thomas.Kuhr@lmu.de"
__requires__ = ["numpy", "matplotlib", "iminuit", "scipy"]

setup(
    name = __project__,
    version = __version__,
    description = __description__,
    packages = __packages__,
    author = __author__,
    author_email = __author_email__,
    requires = __requires__,
)
