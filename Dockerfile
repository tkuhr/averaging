FROM cern/c8-base

MAINTAINER Thomas.Kuhr@lmu.de
RUN yum update -y && yum install -y python3 python3-pip git gcc-c++ cmake
RUN python3 -m pip install numpy matplotlib scipy iminuit==1.5.4
COPY hflav_* /usr/local/bin/
COPY setup.py /tmp/
RUN mkdir /tmp/averaging
COPY averaging/* /tmp/averaging/
RUN cd /tmp && python3 setup.py install
