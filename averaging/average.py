#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape
import re
import os
import shutil
import glob
import math
import subprocess
import getpass
from functools import partial
from pprint import pprint
from contextlib import redirect_stdout
try:
    import numpy as np
except ImportError as exception:
    print('ERROR: The averaging module requires numpy.')
    raise exception
try:
    import matplotlib
except ImportError as exception:
    print('ERROR: The averaging module requires matplotlib.')
    raise exception
try:
    import matplotlib.pyplot as plt
except Exception as exception:
    print('ERROR: The import of matplotlib.pyplot failed:')
    print(exception)
    print('Do you have a connection with X forwarding enabled?')
    print('Maybe select another backend in ./matplotlibrc?')
    raise exception
try:
    from iminuit import Minuit
except ImportError as exception:
    print('ERROR: The averaging module requires iminuit.')
    raise exception
try:
    from scipy import stats
except ImportError as exception:
    print('ERROR: The averaging module requires scipy.')
    raise exception
from .tools import child_text, child_float, get_value, find_reg

debug = False
show_superseded = 'web'
subtitle = getpass.getuser()
parameters = {}
inputs = {}
publications = []
bibtex = {}

experiment_colors = {
    'ATLAS': 'black',
    'BaBar': 'green',
    'Belle': 'blue',
    'CDF': 'red',
    'CMS': 'orange',
    'D0': 'magenta',
    'LHCb': 'cyan',
    }
mathjax_url = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML'


def sym_gauss(mean, sigma, x):
    """Symmetric Gaussian function"""

    d = (x - mean) / sigma
    return d * d


def asym_gauss(mean, sigma_pos, sigma_neg, x):
    """Asymmetric Gaussian function with variable width linear in variance, see arXiv:physics/0406120.
    For deviations of more than sigma_+(-) the variance is fixed to sigma_+(-)^2."""

    d = x - mean
    V = sigma_pos * sigma_neg + (sigma_pos - sigma_neg) * d
    Vmin = min(sigma_pos * sigma_pos, sigma_neg * sigma_neg)
    if V < Vmin:
        V = Vmin
    Vmax = max(sigma_pos * sigma_pos, sigma_neg * sigma_neg)
    if V > Vmax:
        V = Vmax
    return d * d / V


def hflav_logo(fig):
    global subtitle

    save_axes = plt.gca()
    scale = 8. / fig.get_figwidth()
    width = 0.15 * scale * scale
    height = 0.55 * scale / fig.get_figheight()
    plt.axes([0, 1-height, width, height])
#    plt.axes([1-width, 1-height, width, height])
    plt.xticks([])
    plt.yticks([])
    fraction = 0.4
    plt.fill([0, 1, 1, 0], [fraction, fraction, 1, 1], facecolor='k', fill=True)
    font1 = {'family' : 'sans-serif',
             'style'  : 'italic',
             'color'  : 'white',
             'weight' : 'normal',
             'size'   : 22 * scale,
             }
    plt.text(0.5, fraction * 1.6, 'HFLAV', fontdict=font1, ha='center', va='center')
    font2 = {'family' : 'sans-serif',
             'style'  : 'italic',
             'color'  : 'black',
             'weight' : 'normal',
             'size'   : 10 * scale,
             }
    plt.fill([0, 1, 1, 0], [0, 0, 1-fraction, 1-fraction], edgecolor='k', fill=False)
    plt.text(0.5, fraction * 0.5, subtitle, fontdict=font2, ha='center', va='center')
    plt.sca(save_axes)


class Error:
    """representation of a symmetric or asymmetric uncertainty.
    The uncertainty has a type and an optional comment."""
    
    def __init__(self, error = 0, error_type = None, comment = None, xml = None):
        """Initialize the uncertainty object with the given values and/or from an xml element."""

        if isinstance(error, list):
            self.sym = None
            self.pos = max(error[0], error[1])
            self.neg = min(error[0], error[1])
        else:
            self.sym = error
            self.pos = None
            self.neg = None
        self.type = error_type
        self.comment = comment

        if xml is None:
            return
            
        if ',' in xml.text:
            self.sym = None
            value1 = float(xml.text.split(',')[0])
            value2 = float(xml.text.split(',')[1])
            self.pos = max(value1, value2)
            self.neg = min(value1, value2)
        else:
            self.sym = float(xml.text)
            self.pos = None
            self.neg = None
        self.type = xml.get('type', None)
        self.comment = xml.get('comment', None)


    def __str__(self):
        """Conversion to string. Useful for debugging."""        

        if self.sym is not None:
            return '+-%f (%s)' % (self.sym, self.type)
        else:
            return '%+f %+f (%s)' % (self.pos, self.neg, self.type)


    def html(self, digits):
        """Conversion to html string."""

        if self.sym is not None:
            return ('+-%.' + str(digits) + 'f (%s)') % (self.sym, self.type)
        else:
            return ('%+.' + str(digits) + 'f %+.' + str(digits) + 'f (%s)') % (self.pos, self.neg, self.type)


    def latex(self, digits):
        """Conversion to latex string."""

        if self.sym is not None:
            return ('\pm %.' + str(digits) + 'f\mbox{ (%s)}') % (self.sym, self.type)
        else:
            return ('^{%+.' + str(digits) + 'f}_{%+.' + str(digits) + 'f}\mbox{ (%s)}') % (self.pos, self.neg, self.type)


    def brief_latex(self, digits, scale):
        """Conversion to latex string."""

        if self.sym is not None:
            return ('\pm %.' + str(digits) + 'f') % (self.sym * scale)
        else:
            return ('\,^{%+.' + str(digits) + 'f}_{%+.' + str(digits) + 'f}') % (self.pos * scale, self.neg * scale)


    def dump(self):
        comment = ''
        if self.comment:
            comment = ' comment="%s"' % self.comment
        if self.sym:
            error = '%f' % self.sym
        else:
            error = '%f,%f' % (self.neg, self.pos)
        print('    <error type="%s"%s>%s</error>' % (self.type, comment, error))


    def positive(self):
        """Positive sign uncertainty"""
        
        if self.pos is not None:
            return self.pos
        return self.sym


    def negative(self):
        """Negative sign uncertainty"""
        
        if self.neg is not None:
            return self.neg
        return -1 * self.sym


    def average(self):
        """Average of positive and negative sign uncertainty"""

        if self.sym is not None:
            return self.sym
        return (self.pos - self.neg) / 2


    def assign(self, error):
        """Assign a symetric uncertainty"""
        
        self.sym = error
        self.pos = None
        self.neg = None


    def assign(self, error_pos, error_neg):
        """Assign an asymetric uncertainty"""
        
        self.sym = None
        self.pos = error_pos
        self.neg = error_neg


    def add(self, error):
        """Add the given error to this uncertainty."""
        
        if self.sym is not None and error.sym is not None:
            self.sym = math.sqrt(self.sym*self.sym + error.sym*error.sym)
        else:
            self.pos = math.sqrt(self.positive()*self.positive() + error.positive()*error.positive())
            self.neg = -1 * math.sqrt(self.negative()*self.negative() + error.negative()*error.negative())
            self.sym = None


    def remove(self, error):
        """Add the given error from this uncertainty."""
        
        if self.sym is not None and error.sym is not None:
            self.sym = math.sqrt(self.sym*self.sym - error.sym*error.sym)
        else:
            self.pos = math.sqrt(self.positive()*self.positive() - error.positive()*error.positive())
            self.neg = -1 * math.sqrt(self.negative()*self.negative() - error.negative()*error.negative())
            self.sym = None


    def norm_chi2_func(self):
        """Chi2 function with an average width of 1"""

        if self.sym is not None:
            return partial(sym_gauss, 0, 1)
        return partial(asym_gauss, 0, self.pos / self.average(), -self.neg / self.average())


class Data:
    """Representation of a data point, which can be either a central value with uncertainties or a 90 CL limit.
    The value is interpreted as a limit if no uncertainty is given."""
    
    def __init__(self, value = None, unit = 1, digits = 0, xml = None):
        """Initialize the data object with the given values and/or from an xml element."""
        
        self.value = value
        self.unit = unit
        self.digits = digits
        
        self.errors = []
        
        if xml is None:
            return
        
        self.value = child_float(xml, 'value')
        value_digits = child_text(xml, 'value').split('.')
        if len(value_digits) == 1:
            self.value_digits = 0
        else:
            self.digits = len(value_digits[1])
        self.unit = get_value(1, child_float(xml, 'unit'))
        self.errors = []
        for child in xml.findall('error'):
            self.errors.append(Error(xml = child))


    def has_unit(self):
        if self.unit:
            return abs(math.log10(self.unit)) > 0.5
        return false


    def __str__(self):
        """Conversion to string. Useful for debugging."""        

        if self.is_limit():
            result = '%f' % self.value
        else:
            result = '[%f' % self.value
            for error in self.errors:
                result += ' %s' % str(error)
            result += ']'
        if self.has_unit():
            result += ' x %f' % self.unit
        return result


    def html(self):
        """Conversion to html string."""

        if self.is_limit():
            result = ('%.' + str(self.digits) + 'f') % self.value
        else:
            result = ('[%.' + str(self.digits) + 'f') % self.value
            for error in self.errors:
                result += ' ' + error.html(self.digits)
            result += ']'
        if self.has_unit():
            result += ' x 10<sup>%d</sup>' % math.log10(self.unit)
        return result


    def latex(self):
        """Conversion to latex string."""

        if self.is_limit():
            result = ('%.' + str(self.digits) + 'f') % self.value
        else:
            result = ('[%.' + str(self.digits) + 'f') % self.value
            for error in self.errors:
                result += ' ' + error.latex(self.digits)
            result += ']'
        if self.has_unit():
            result += ' \\times 10^{%d}' % math.log10(self.unit)
        return result


    def brief_latex(self, unit):
        """Conversion to brief latex string."""

        scale = self.get_unit() / unit
        digits = max(0, self.digits - int(math.log10(scale)))
        if self.is_limit():
            result = ('< %.' + str(digits) + 'f') % (self.value * scale)
        else:
            result = ('%.' + str(digits) + 'f') % (self.value * scale)
            syst_error = Error(0)
            for error in self.errors:
                if error.type == 'stat':
                    result += ' ' + error.brief_latex(digits, scale)
                else:
                    syst_error.add(error)
            result += ' ' + syst_error.brief_latex(digits, scale)
        return result


    def dump(self):
        print('    <value>%f</value>' % self.value)
        if self.has_unit():
            print('    <unit>%f</unit>' % self.unit)
        for error in self.errors:
             error.dump()


    def get_value(self):
        return self.value * self.get_unit()


    def is_limit(self):
        """Is the data point a limit or central value with uncertainties."""
        
        return len(self.errors) == 0


    def get_unit(self):
        if self.unit:
            return self.unit
        return 1


    def pos_total(self):
        """Total positive sign uncertainty"""
        
        sum2 = 0
        for error in self.errors:
            sum2 += error.positive()*error.positive()
        return math.sqrt(sum2)


    def neg_total(self):
        """Total negative sign uncertainty"""
        
        sum2 = 0
        for error in self.errors:
            sum2 += error.negative()*error.negative()
        return -math.sqrt(sum2)


    def pos_stat_syst(self):
        """Statistical plus experimental systematic positive sign uncertainty"""
        
        sum2 = 0
        for error in self.errors:
            if error.type in ['stat', 'syst']:
                sum2 += error.positive()*error.positive()
        return math.sqrt(sum2)


    def neg_stat_syst(self):
        """Statistical plus experimental systematic negative sign uncertainty"""
        
        sum2 = 0
        for error in self.errors:
            if error.type in ['stat', 'syst']:
                sum2 += error.negative()*error.negative()
        return -math.sqrt(sum2)


    def pos_stat(self):
        """Statistical positive sign uncertainty"""
        
        for error in self.errors:
            if error.type == 'stat':
                return error.positive()
        return 0


    def neg_stat(self):
        """Statistical negative sign uncertainty"""
        
        for error in self.errors:
            if error.type == 'stat':
                return error.negative()
        return 0


    def symmetric_errors(self):
        """True if all errors are symmetric"""

        for error in self.errors:
            if error.sym is None:
                return False
        return True


    def chi2_func(self):
        """Chi2 function for statistical part"""

        for error in self.errors:
            if error.type.startswith('stat'):
                if error.sym is not None:
                    return partial(sym_gauss, self.value * self.get_unit(), error.sym * self.get_unit())
                else:
                    return partial(asym_gauss, self.value * self.get_unit(), error.pos * self.get_unit(), abs(error.neg) * self.get_unit())
        return None


class Input:
    """Representation of an input parameter value that is used in the measurement of another parameter.
    The name of the input parameter is given by param and should correspond to a known input parameter.
    The variables variation_up/down specify the deviations from the central input value that led to the corresponding positive/negative systematic error. Note that the signs of variation_up/down matter and _up corresponds to the positive and _down to the negative systemtic uncertainty."""
    
    def __init__(self, xml):
        """Initialize the input parameter value object from an xml element."""
        
        self.type = xml.get('type', None)
        self.param = child_text(xml, 'param')
        self.value = child_float(xml, 'value')
        self.unit = get_value(1, child_float(xml, 'unit'))
        variation = child_text(xml, 'variation')
        if ',' in variation:
            self.variation_up = float(variation.split(',')[0])
            self.variation_down = float(variation.split(',')[1])
        else:
            self.variation_up = float(variation)
            self.variation_down = -self.variation_up


def syst_shift(factor, param_index, x):
    return factor * x[param_index]


class NuissanceSystematics:
    """Nuissance parameters for systematic uncertainties"""

    class Nuissance:
        def __init__(self, chi2_func, param_index):
            self.chi2_func = chi2_func
            self.param_index = param_index

    def __init__(self, param_offset = 0):
        self.param_offset = param_offset
        self.nuissances = []
        self.systematics_names = {}

    def size(self):
        return len(self.nuissances)

    def add(self, error, unit):
        param_index = len(self.nuissances) + self.param_offset
        if error.type.startswith('stat'):
            return None
        if error.type in self.systematics_names.keys():
            param_index = self.systematics_names[error.type]
        else:
            self.nuissances.append(self.Nuissance(error.norm_chi2_func(), param_index))
            if error.type != 'syst':
                self.systematics_names[error.type] = param_index
        return partial(syst_shift, error.average() * unit, param_index)

    def chi2(self, x):
        result = 0
        for nuissance in self.nuissances:
            result += nuissance.chi2_func(x[nuissance.param_index])
        return result


class DataChi2:
    """functions to calculate the statistical chi2 contribution of a measurement with systematic shifts"""

    def __init__(self, chi2_func):
        self.chi2_func = chi2_func
        self.shifts = []

    def add_shift(self, shift):
        self.shifts.append(shift)

    def get_shift(self, x):
        result = 0
        for shift in self.shifts:
            result += shift(x)
        return result

    def chi2(self, x):
        return self.chi2_func(x[0] - self.get_shift(x))


class Parameter:
    """Representation of a measured parameter."""

    def __init__(self, name, xml):
        """Initialize the parameter object with the given name and from an xml element."""

        self.name = name
        if xml.tag == 'parameter':
            self.latex = child_text(xml, 'latex').strip()
            self.pdglive = child_text(xml, 'pdglive')
            self.comment = child_text(xml, 'comment')
            self.unit = child_float(xml, 'unit')
            self.data = None
            if xml.find('value') is not None:
                self.data = Data(xml = xml)
            self.cl = None
        self.measurements = []
        self.selected = True

        
    def __str__(self):
        """Conversion to string. Useful for debugging."""        

        return self.name


    def dump(self):
        print('<parameter>')
        if self.latex:
            print('  <latex>%s</latex>' % self.latex)
        if self.pdglive:
            print('  <pdglive>%s</pdglive>' % self.pdglive)
        if self.comment:
            print('  <comment>%s</comment>' % self.comment)
        if self.data:
            self.data.dump()
        print('</parameter>')


    def long_latex(self):
        """A latex code where \\frac is replaced by /."""
        latex = self.latex
        if latex.startswith('\\frac'):
            latex = latex[5:].strip()
            bracket = 0
            for index in range(len(latex)):
                if latex[index] == '{':
                    bracket += 1
                elif latex[index] == '}':
                    bracket -= 1
                    if bracket == 0:
                        numerator = latex[:index+1]
                        if 'times' in numerator:
                            numerator = '[' + numerator + ']'
                        rest = latex[index+1:]
                        break
            for index in range(len(rest)):
                if rest[index] == '{':
                    bracket += 1
                elif rest[index] == '}':
                    bracket -= 1
                    if bracket == 0:
                        denominator = rest[:index+1]
                        if 'times' in denominator:
                            denominator = '[' + denominator + ']'
                        rest = rest[index+1:]
                        break
            latex = numerator + '/' + denominator
            if len(rest.strip()) > 0:
                latex = '[' + latex + ']' + rest
        return latex

    
    def average_combos(self):
        """Determine the average by passing all measurments to COMBOS."""

        if len(self.measurements) == 0:
            print('WARNING: no measurements for %s' % self.name)
            return
        
        # determine the unit of the average
        for meas in self.measurements:
            unit = meas.data.get_unit()
            if not self.unit or self.unit > unit:
                self.unit = unit
        
        # check whether only limits are available
        measurements = []
        bestlimit = 1e10
        digits = 0
        for meas in self.measurements:
            if meas.superseded or meas.publication.superseded:
                continue
            if not meas.data.is_limit():
                measurements.append(meas)
            if meas.data.get_value() < bestlimit:
                bestlimit = meas.data.get_value()
                digits = meas.data.digits - int(math.log10(meas.data.get_unit() / self.unit))
        if len(measurements) == 0:
            self.data = Data(value = bestlimit / self.unit, unit = self.unit, digits = max(digits, 0))
            return
            
        # check whether only one measurmeent is available
        if len(measurements) == 1:
            data = measurements[0].data
            self.data = Data(value = data.value, unit = data.unit, digits = data.digits)
            sym = True
            for error in data.errors:
                if error.sym is None:
                    sym = False
            if sym:
                self.data.errors.append(Error(data.pos_total(), 'combined'))
            else:
                self.data.errors.append(Error([data.pos_total(), data.neg_total()], 'combined'))
            return

        # determine an offset to work around the issue that combos cannot handle negative input values
        offset = 0
        for meas in measurements:
            unit = meas.data.get_unit() / get_value(1, self.unit)
            if meas.data.value * unit < offset:
                offset = meas.data.value * unit

        # if combos is not available try to get the combos result for the log file
        if not os.path.exists('log'):
            os.makedirs('log')
        log_name = os.path.join('log', os.path.basename(self.name))
        if not shutil.which('combos'):
            if not os.path.exists(log_name + '.output'):
                print('ERROR: neither COMBOS nor the COMBOS output file for %s are available' % self.name)
                return
            
            output_file = open(log_name + '.output', 'r')
            out = output_file.read()
            output_file.close()

        else:
            # prepare the combos input card
            combos = ''
            uids = []
            for meas in measurements:
                while meas.uid() in uids:
                    meas.method = get_value('', meas.method) + 'x'
                uids.append(meas.uid())
                meas_digits = meas.data.digits - int(math.log10(meas.data.get_unit() / self.unit))
                if meas_digits > digits:
                    digits = meas_digits
                unit = meas.data.get_unit() / get_value(1, self.unit)
                meas.data.value -= offset / unit
                combos += meas.combos() + '\n'
                meas.data.value += offset / unit
            combos += """BEGIN HFLAV ALL COMBINED
COMBINE * * *
MEASUREMENT value
CALL CHI2_SYM_MIN 21
MINOS ALL
END
"""

            # run combos
            process = subprocess.Popen(['combos'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            (combos_out, combos_err) = process.communicate(str.encode(combos))
            out = combos_out.decode()

            # log input and output
            input_file = open(log_name + '.input', 'w')
            input_file.write(combos)
            input_file.close()
            output_file = open(log_name + '.output', 'w')
            output_file.write(out)
            output_file.close()

            # check whether combos failed or reported any errors
            if process.returncode != 0:
                print('*** COMBOS exited with error code %d' % process.returncode)
                print('*** COMBOS input:\n', combos)
                print('*** COMBOS output:\n', out)
                return
            combos_errors = find_reg(out, ' Total number of ERROR   messages generated by COMBOS:\\s*(\\d+)')
            if combos_errors != None:
                print('*** COMBOS reported %s errors' % combos_errors)
                print('*** COMBOS input:\n', combos)
                print('*** COMBOS output:\n', out)

        # get the result from the combos output
        result = find_reg(out, ' CHI2_SYM_MIN    :           VALUE=\\s*([+-\.\\d]+)\\s+([+-\.\\d]+)\\s+([+-\.\\d]+)\\s+CL=([\.\\d]+)')
        if result is None:
            result = find_reg(out, ' CHI2_SYM_MIN    :           VALUE=\\s*([+-\.\\d]+)\\s+([+-\.\\d]+)\\s+([+-\.\\d]+)')
        if result is None:
            print('*** No COMBOS result found:\n', out)
            return

        self.data = Data(value = float(result[0]) + offset, unit = self.unit, digits = digits)
        if result[1] == '+-':
            self.data.errors.append(Error(float(result[2]), 'combined'))
        else:
            self.data.errors.append(Error([float(result[2]), float(result[1])], 'combined'))
        if len(result) > 3:
            self.cl = float(result[3])


    def average(self):
        """Determine the average of all measurments."""

        if len(self.measurements) == 0:
            print('WARNING: no measurements for %s' % self.name)
            return

        # determine the unit of the average
        for meas in self.measurements:
            unit = meas.data.get_unit()
            if not self.unit or self.unit > unit:
                self.unit = unit

        # check whether only limits are available
        measurements = []
        bestlimit = 1e10
        digits = 0
        for meas in self.measurements:
            if meas.superseded or meas.publication.superseded:
                continue
            if not meas.data.is_limit():
                measurements.append(meas)
            if meas.data.get_value() < bestlimit:
                bestlimit = meas.data.get_value()
                digits = meas.data.digits - int(math.log10(meas.data.get_unit() / self.unit))
        if len(measurements) == 0:
            self.data = Data(value = bestlimit / self.unit, unit = self.unit, digits = max(digits, 0))
            return

        # check whether only one measurmeent is available
        if len(measurements) == 1:
            data = measurements[0].data
            self.data = Data(value = data.value, unit = data.unit, digits = data.digits)
            if data.symmetric_errors():
                self.data.errors.append(Error(data.pos_total(), 'combined'))
            else:
                self.data.errors.append(Error([data.pos_total(), data.neg_total()], 'combined'))
            return

        # get the chi2 functions:
        # each measurement contributes one DataChi2 for the statistical part
        # systematic uncerainties are included by shifts with (asymmetric) Gaussian constraints
        data_chi2s = []
        nuissances = NuissanceSystematics(param_offset = 1)
        digits = 0
        sym = True
        for meas in measurements:
            meas_digits = meas.data.digits - int(math.log10(meas.data.get_unit() / self.unit))
            if meas_digits > digits:
                digits = meas_digits
            unit = meas.data.get_unit() / self.unit

            data_chi2 = DataChi2(meas.data.chi2_func())
            data_chi2s.append(data_chi2)
            for error in meas.data.errors:
                shift = nuissances.add(error, meas.data.get_unit())
                if shift is not None:
                    data_chi2.add_shift(shift)
                if error.sym is None:
                    sym = False

        # function that is minimized
        def chi2(x):
            result = 0
            for data_chi2 in data_chi2s:
                result += data_chi2.chi2(x)
            return result + nuissances.chi2(x)

        # run minuit
        initials = [measurements[0].data.get_value()] + [0] * nuissances.size()
        steps = [measurements[0].data.pos_stat() * measurements[0].data.get_unit()] + [1] * nuissances.size()
        if not os.path.exists('log'):
            os.makedirs('log')
        log_name = os.path.join('log', os.path.basename(self.name) + '.log')
        with open(log_name, 'w') as log:
            with redirect_stdout(log):
                minuit = Minuit.from_array_func(chi2, initials, steps, errordef=1)
                fmin, param = minuit.migrad()

        # check whether minimization failed
        if not fmin.is_valid:
            print('*** migrad failed')
            pprint(fmin)
            return

        # get results
        self.data = Data(value = param[0].value / self.unit, unit = self.unit, digits = digits)
        self.cl = 1 - stats.chi2.cdf(fmin.fval, 1)
        if sym:
            self.data.errors.append(Error(param[0].error / self.unit, 'combined'))

        else:
            # run minos if we have asymmetric errors
            with open(log_name, 'a') as log:
                with redirect_stdout(log):
                    merrors = minuit.minos('x0')['x0']
            if not merrors.is_valid:
                print('*** minos failed')
                pprint(merrors)
                return
            neg = merrors.lower / self.unit
            pos = merrors.upper / self.unit
            format_str = '%+.' + str(digits) + 'f'
            if (format_str % neg)[1:] == (format_str % neg)[1:]:
                self.data.errors.append(Error((pos - neg) / 2, 'combined'))
            else:
                self.data.errors.append(Error([neg, pos], 'combined'))


    def plot(self):
        """Create a plot of average and individual measurements."""

        # don't include superseded results unless selected by show_superseded
        measurements = []
        for meas in self.measurements:
            if show_superseded == 'all' or not (meas.superseded or meas.publication.superseded):
                measurements.append(meas)

        # create the plot axes
        plot_unit = self.data.get_unit()
        n = len(measurements)
        dy = 1. / (n + 1)
        fig = plt.figure(figsize=(8, (n+3) * 0.4))
        latex = self.long_latex()
        title = r'$' + latex + '$'
        if plot_unit != 1:
            title += r' $[10^{%d}]$' % math.log10(plot_unit)
        plt.title(title)
        plt.axis(ymin=0, ymax=1)
        plt.yticks([])
        xzero = False

        # plot the average
        if self.data.is_limit():
            plt.bar(0, 1, self.data.value, 0, align='edge', color='yellow')
        else:
            plt.axvspan(self.data.value + self.data.neg_total(), self.data.value + self.data.pos_total(), color='yellow')
            plt.axvline(self.data.value, color='black')
        
        # plot each measurement
        y = dy
        legend_entries = []
        legend_labels = []
        for meas in measurements:
            unit = meas.data.get_unit() / plot_unit
            global experiment_colors
            color = experiment_colors[meas.publication.experiment]
            linestyle = 'solid'
            if meas.superseded or meas.publication.superseded:
                linestyle = '‘dashed’'
            if meas.data.is_limit():
                plot = plt.errorbar([meas.data.value * unit], [y], xerr=[[meas.data.value * unit], [0]], xuplims=True, capsize=6, mew=2, color=color, linestyle=linestyle)
                plot[1][0].set_marker(matplotlib.markers.CARETLEFT)
                plot[1][0].set_markeredgewidth(0)
                if xzero is False:
                    xzero = True
            else:
                plot = plt.errorbar([meas.data.value * unit], [y], xerr=[[abs(meas.data.neg_total() * unit)], [meas.data.pos_total() * unit]], fmt='o', capsize=0, color=color, linestyle=linestyle)
                plt.errorbar([meas.data.value * unit], [y], xerr=[[abs(meas.data.neg_stat() * unit)], [meas.data.pos_stat() * unit]], capsize=6, mew=2, color=color)
                if len(meas.data.errors) > 2:
                    plt.errorbar([meas.data.value * unit], [y], xerr=[[abs(meas.data.neg_stat_syst() * unit)], [meas.data.pos_stat_syst() * unit]], capsize=4, color=color)
                if meas.data.value + meas.data.neg_total() < 0:
                    xzero = None
            if meas.publication.experiment not in legend_labels:
                legend_labels.insert(0, meas.publication.experiment)
                legend_entries.insert(0, plot)
                        
            y += dy

        if xzero:
            plt.xlim(xmin=0)
        # draw legend, logo and save figure
        try:
            plt.legend(legend_entries, legend_labels)
        except:
            print('WARNING: Plotting of legend for %s failed.' % self.name)
        hflav_logo(fig)
        try:
            plt.tight_layout()
        except:
            pass
        filename = os.path.join('html', self.name) + '.png'
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        try:
            plt.savefig(filename, format='png')
        except Exception as exception:
            print('WARNING: The saving of %s failed:' % filename)
            print(exception)
        plt.close()


    def html(self):
        """Create a html page of average and individual measurements."""

        # open the html file
        filename = os.path.join('html', self.name) + '.html'
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        html = open(filename, 'w')
        basename = os.path.basename(self.name)
        html.write("""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>%s</title>
<script type="text/javascript"
  src="%s">
</script>
<link rel="stylesheet" type="text/css" href="../../style.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
    $(function(){
      $("#nav-placeholder").load("../../navbar.html");
    });
</script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<meta name="theme-color" content="#fafafa">
</head>
<body>
<div id="nav-placeholder"></div>
""" % (basename, mathjax_url))

        # figure
        html.write('<p><img src="%s.png"></p>\n' % basename)

        # table
        html.write('<table border="1"><tr><th>Experiment</th><th>Measurement</th><th>Reference</th></tr>\n')
        average_unit = self.data.get_unit()
        
        # measurements
        for meas in reversed(self.measurements):
            if show_superseded == 'none' and (meas.superseded or meas.publication.superseded):
                continue
            unit = meas.data.get_unit() / average_unit
            global experiment_colors
            color = experiment_colors[meas.publication.experiment]
            html.write('<tr><td>%s</td><td>' % meas.publication.experiment)
            if meas.data.is_limit():
                html.write('&lt; ')
            href = meas.publication.href()
            ref = meas.publication.reference
            if meas.superseded or meas.publication.superseded:
                ref += ' (superseded)'
            if href:
                html.write('\\(%s\\)</td><td><a href="%s">%s</a></td></tr>\n' % (meas.data.latex(), href, ref))
            else:
                html.write('\\(%s\\)</td><td>%s</td></tr>\n' % (meas.data.latex(), ref))
                
        # average
        unit_label = ''
        if average_unit != 1:
            unit_label = ' x 10<sup>%d</sup>\n' % math.log10(average_unit)
        html.write('<tr><td><b>Average</b></td><td><b>')
        if self.data.is_limit():
            html.write('&lt; ')
        html.write(' \\(\\mathbf{%s}\\)</b></td>' % self.data.latex())
        if self.cl is not None:
            html.write('<td>CL=%.4f</td></tr></table>\n' % self.cl)
        else:
            html.write('<td></td></tr></table>\n')
        
        # debug output
        global debug
        if debug:
            parname = os.path.split(self.name)[-1]
            html.write('<h2>DEBUG INFORMATION</h2>\n')
            html.write('Parameter: <a href="../../../parameters/%s.xml">parameters/%s.xml</a><br>\n' % (self.name, self.name))
            if os.path.exists('log/%s.log' % parname):
                html.write('MINUIT: <a href="../../../log/%s.log">log</a><br>\n' % (parname))
            if os.path.exists('log/%s.input' % parname):
                html.write('COMBOS: <a href="../../../log/%s.input">input</a>, <a href="../../../log/%s.output">output</a><br>\n' % (parname, parname))
            else:
                html.write('COMBOS: not used<br>\n')
            html.write('Measurements:<br>\n')
            for meas in reversed(self.measurements):
                html.write('%s: <a href="../../../measurements/%s/%s">measurements/%s/%s</a><br>\n' % (meas.publication.experiment, meas.publication.experiment, meas.publication.filename,  meas.publication.experiment, meas.publication.filename))
        
        # close html file
        html.write('</body>\n</html>\n')
        html.close()


    def tex(self, unit):
        """Generate the latex code to be included in a table."""

        latex_exp = {'BaBar': '\\babar',
                     'D0': '\\dzero'}
        unit_str = ''
        if unit is None:
            unit_str = ' $[10^{%d}]$' % math.log10(self.unit)
            unit = self.unit
        
        result = ' & \\begin{tabular}{l} '
        for meas in reversed(self.measurements):
            if show_superseded in ['none', 'web'] and (meas.superseded or meas.publication.superseded):
                continue
            result += '%s' % latex_exp.get(meas.publication.experiment, meas.publication.experiment)
            if meas.publication.bibtex:
                bibtex_id = find_reg(meas.publication.bibtex, '@\w+\{(\S+),\n[\S\s]*')
                if bibtex_id not in list(bibtex.keys()):
                    bibtex[bibtex_id] = meas.publication.bibtex
                result += ' \\cite{%s}' % bibtex_id
            if meas.superseded or meas.publication.superseded:
                result += ' (sup.)'
            result += ': $%s$ \\\\ ' % meas.data.brief_latex(unit)
        result += '\\end{tabular} & '
        if self.cl is not None and self.cl < 0.01:
            result += '\\begin{tabular}{@{}l@{}} $%s$ \\\\[-5pt] {\\tiny CL=%.1f\\permil} \\end{tabular} \\\\\n' % (self.data.brief_latex(unit), 1000*self.cl)
        else:
            result += '$%s$ \\\\\n' % self.data.brief_latex(unit)
        param_cmd = '\\btocharmparam'
        try:
            text_obj = matplotlib.textpath.TextPath((0,0), '$%s$%s' % (self.latex, unit_str), usetex=True)
            if text_obj.get_extents().width > 150:
                param_cmd = '\\btocharmparaml'
            else:
                test_str = ('$%s$%s%s' % (self.latex, unit_str, result)).replace('&', '\\quad')
                text_obj = matplotlib.textpath.TextPath((0,0), test_str, usetex=True)
                if text_obj.get_extents().width > 300:
                    param_cmd = '\\btocharmparaml'
        except:
            pass
        return '%s{$%s$}{$%s$}{%s}%s' % (param_cmd, self.latex, self.long_latex(), unit_str, result)
        

class Measurement:
    """Representation of a measurement."""
    
    def __init__(self, xml, publication):
        """Initialize the measurement object from an xml element and a given publication."""
        
        self.param = child_text(xml, 'param')
        if not self.param:
            print('ERROR: a parameter identifier is missing for a measurement in %s' % publication.filename)
            return

        self.data = Data(xml = xml)
        self.method = child_text(xml, 'method')
        self.comment = child_text(xml, 'comment')
        self.publication = publication
        self.superseded = child_text(xml, 'superseded')
        if not self.superseded:
            self.superseded = xml.find('superseded') is not None

        self.inputs = {}
        for child in xml.findall('input'):
            inp = Input(child)
            self.inputs[inp.param] = inp

        global parameters
        if self.param in list(parameters.keys()):
            parameters[self.param].measurements.append(self)
            parameters[self.param].measurements.sort(key=lambda meas: meas.publication.experiment + meas.publication.filename)
        else:
            print('ERROR: unknown parameter %s' % self.param)


    def __str__(self):
        """Conversion to string. Useful for debugging."""        

        if self.data.is_limit():
            result = '%s < ' % self.param
        else:
            result = '%s = ' % self.param
        result += self.data
        if self.superseded:
            result += ' superseded'
            if isinstance(self.superseded, str):
                result += ' by %s' % self.superseded
        if self.comment:
            result += ' {%s}' % self.comment
        return result


    def dump(self):
        print('  <measurement>')
        print('    <param>%s</param>' % self.param)
        self.data.dump()
        if self.comment:
            print('    <comment>%s</comment>' % self.comment)
        if self.superseded:
            if isinstance(self.superseded, str):
                print('    <superseded>%f</superseded>' % self.superseded)
            else:
                print('    <superseded/>')
        print('  </measurement>')


    def uid(self):
        """Get an identifier of this measurement."""
        
        uid = self.publication.experiment
        if self.method or self.publication.method:
            uid += '_' + get_value(self.method, self.publication.method)
        return uid

    
    def combos(self):
        """Get the combos input card for this measurement."""
        
        result = 'BEGIN %s %s STATUS\n' % (self.publication.experiment,
                                           get_value('METHOD', self.method, self.publication.method))
        result += 'MEASUREMENT value stat total_syst\n'

        # determine unit scale factor
        global parameters
        unit = self.data.get_unit() / get_value(1, parameters[self.param].unit)

        # central value
        result += 'DATA value %g\n' % (self.data.value * unit)

        # create errors dictionary
        errors = {}
        for error in self.data.errors:
            errors[error.type] = error

        # loop over norm/factor input parameters and calculate their error contribution
        for name, inp in self.inputs.items():
            if inp.type == 'norm':
                error_pos = inp.variation_up * data.value / inp.value * unit
                error_neg = inp.variation_down * data.value / inp.value * unit
            elif inp.type == 'factor':
                error_pos = inp.variation_down * data.value / inp.value * unit
                error_neg = inp.variation_up * data.value / inp.value * unit
            else:
                continue
                                     
            # add errors for parameters for which no explicit error is given and remove their contribution from the experimental systematic one
            if name not in list(errors.keys()):
                error = Error([error_pos, error_neg], name)
                #error.unit = inp.unit
                errors.append(error)
                errors['syst'].remove(error)
                
            # adjust errors for given norm/factor parameters 
            else:
                error = errors[name]
                if abs((error_pos - error.positive()) / error.positive()) > 0.3:
                    print('WARNING: positive error of type %s changed from %g to %g.' % (error.type, error.positive(), error_pos))
                if abs((error_neg - error.negative()) / error.negative()) > 0.3:
                    print('WARNING: negative error of type %s changed from %g to %g.' % (error.type, error.negative(), error_neg))
                error.assign(error_pos, error_neg)
        
        # calculate total systematic error
        total_syst = Error(error_type = 'total_syst')
        for error in list(errors.values()):
            if error.type != 'stat':
                total_syst.add(error)
        if total_syst.positive() > 0:
            errors[total_syst.type] = total_syst

        # errors        
        for error in list(errors.values()):
            name = error.type.replace(' ', '_')[:15]
            while name[-1] in ['+', '-']:
                name = name[:-1]
            if name == 'syst':
                name += '_' + self.uid()
            if error.sym is not None:
                result += 'DATA %s %g\n' % (name, error.sym * unit)
            else:
                result += 'DATA %s+ %s- %g %g\n' % (name, name, error.pos * unit, error.neg * unit)

        # input parameters
        if len(list(self.inputs.keys())) > 0:
            result += 'PARAMETERS\n'
            data_names = 'DATA\n'
            data_values = '\n'
            for name, inp in self.inputs.items():
                result += '%s %g %g %g\n' % (name, inp.value, inp.variation_up, inp.variation_down)
                data_names += '%s+ %s- ' % (name, name)
                # data_values += ...
                # error_type
            result += data_names + data_values
        result += 'END\n'
        return result


class Publication:
    """Representation of a publication or preliminary document with one or more measurements."""
    
    def __init__(self, experiment, filename, xml):
        """Initialize the measurement object from an xml element and a gievn experiment name."""

        self.experiment = experiment
        self.filename = filename
        self.reference = child_text(xml, 'reference')
        self.preprint = child_text(xml, 'preprint')
        self.inspire = child_text(xml, 'inspire')
        self.doi = child_text(xml, 'doi')
        self.link = child_text(xml, 'link')
        self.method = child_text(xml, 'method')
        self.comment = child_text(xml, 'comment')
        self.published = xml.find('unpublished') is None
        self.superseded = child_text(xml, 'superseded')
        if not self.superseded:
            self.superseded = xml.find('superseded') is not None
        self.bibtex = child_text(xml, 'bibtex')
        self.measurements = []
    
        for child in xml.findall('measurement'):
            meas = Measurement(child, self)
            if meas.param:
                self.measurements.append(meas)
        self.selected = True


    def html(self):
        """Create a html page of the publication and the contained measurements."""

        # open the html file
        htmlfilename = os.path.join('html', 'publications', self.experiment, os.path.splitext(self.filename.replace(':', '.'))[0] + '.html')
        if not os.path.exists(os.path.dirname(htmlfilename)):
            os.makedirs(os.path.dirname(htmlfilename))
        html = open(htmlfilename, 'w')
        html.write("""<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>%s</title>
<script type="text/javascript"
  src="%s">
</script>
<link rel="stylesheet" type="text/css" href="../../style.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
    $(function(){
      $("#nav-placeholder").load("../../navbar.html");
    });
</script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<meta name="theme-color" content="#fafafa">
</head>
<body>
<div id="nav-placeholder"></div>
""" % (self.reference, mathjax_url))

        # publication information
        html.write('<h2>%s</h2>\n' % self.reference)
        html.write('<p>%s' % self.experiment)
        if not self.published:
            html.write(' (unpublished)')
        html.write(': ')
        if self.inspire:
            html.write(' <a href=http://inspirehep.net/record/%s>INSPIRE</a>' % self.inspire)
        elif self.preprint:
            html.write(' <a href=http://arxiv.org/abs/%s>arXiv:%s</a>' % (self.preprint, self.preprint))
        elif self.doi:
            html.write(' <a href=http://dx.doi.org/%s>DOI::%s</a>' % (self.doi, self.doi))
        if self.link:
            html.write(' <a href=%s>link</a>' % self.link)
        html.write('</p>\n')
        if self.superseded:
            if isinstance(self.superseded, str):
                html.write('<p>Superseded by: %s</p>' % self.superseded)
            else:
                html.write('<p>Superseded</p>')
        if self.comment:
            html.write('<p>Comment: %s</p>' % self.comment)

        # measurements
        global parameters
        html.write('<table border="1"><tr><th>Parameter</th><th>Measurement</th></tr>\n')
        for meas in self.measurements:
            try:
                param = parameters[meas.param]
            except KeyError:
                print('ERROR: The parameter %s is not defined, but used in %s/%s.' % (meas.param, self.experiment, self.filename))
            html.write('<tr><td><a href="../../%s.html">\(%s\)</a></td><td>\(%s\)' % (meas.param, param.latex, meas.data.latex()))
            if meas.superseded or meas.publication.superseded:
                html.write(' (superseded)')
            html.write('</td></tr></p>\n')
        html.write('</table>\n')

        # debug output
        global debug
        if debug:
            html.write('<h2>DEBUG INFORMATION</h2>\n')
            html.write('%s: <a href="../../../measurements/%s/%s">measurements/%s/%s</a><br>\n' % (self.experiment, self.experiment, self.filename,  self.experiment, self.filename))

        # close html file
        html.write('</body>\n</html>\n')
        html.close()


    def href(self):
        """Web link of the publication."""

        if self.inspire:
            return 'http://inspirehep.net/record/' + self.inspire
        elif self.preprint:
            return 'http://arxiv.org/abs/' + self.preprint
        elif self.doi:
            return 'http://dx.doi.org/' + self.doi
        else:
            return self.link

        
    def dump(self):
        print('<publication>')
        if self.reference:
            print('  <reference>%s</reference>' % self.reference)
        if self.preprint:
            print('  <preprint>%s</preprint>' % self.preprint)
        if self.inspire:
            print('  <inspire>%s</inspire>' % self.inspire)
        if self.doi:
            print('  <doi>%s</doi>' % self.doi)
        if self.link:
            print('  <link>%s</link>' % self.link)
        if self.comment:
            print('  <comment>%s</comment>' % self.comment)
        if not self.published:
            print('  <unpublished/>n')
        for measurement in self.measurements:
            measurement.dump()
        print('</publication>')


def read_parameters(topdir, parameters):
    """Read the xml files in the given directory and its subdirectories, extract parameter information, and add it to the given dictionary of parameters."""
    
    for (dirname, dirs, files) in os.walk(topdir):
        for filename in files:
            if filename.endswith('.xml'):
                filepath = os.path.join(dirname, filename)
                try:
                    tree = ET.parse(filepath)
                except Exception as exception:
                    print('ERROR: The parsing of %s failed: %s' % (filepath, exception))
                    continue
                root = tree.getroot()
                if root.tag == 'parameter':
                    name = os.path.splitext(os.path.relpath(filepath, topdir))[0]
                    parameters[name] = Parameter(name, root)



def read_measurements(topdir, publications):
    """Read the xml files in the given directory and its subdirectories, extract publication and measurements information, and add it to the given list of publications."""

    for (dirname, dirs, files) in os.walk(topdir):
        for filename in files:
            if filename.endswith('.xml'):
                filepath = os.path.join(dirname, filename)
                try:
                    tree = ET.parse(filepath)
                except Exception as exception:
                    print('ERROR: The parsing of %s failed: %s' % (filepath, exception))
                    continue
                root = tree.getroot()
                if root.tag == 'publication':
                    experiment = os.path.split(os.path.relpath(filepath, topdir))[0]
                    publications.append(Publication(experiment, filename, root))


def apply_selection(arguments, git, parameters, publications):
    """Apply a selection if arguments or a git reference are given"""

    selected_files = []

    # get modified files from git diff
    if git is not None:
        process = subprocess.Popen(('git diff --name-only --diff-filter=ACMRT %s' % git).split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        (git_out, git_err) = process.communicate()
        if process.returncode != 0:
            print('ERROR: The git diff command for ref %s failed:' % git_out)
            print(git_out.decode())
            sys.exit(1)
        selected_files = [filename for filename in git_out.decode().splitlines() if filename.endswith('.xml')]

    # add files from command line
    for arg in arguments:
        if os.path.isfile(arg):
            if arg.endswith('.xml'):
                selected_files.append(arg)
        elif os.path.isdir(arg):
            for root, dirs, filenames in os.walk(arg):
                selected_files += [os.path.join(root, filename) for filename in filenames if filename.endswith('.xml')]
        else:
            print('ERROR: The argument %s is not a file or directory' % arg)
            sys.exit(1)

    if len(selected_files) == 0:
        return
    for param in parameters.values():
        param.selected = False
    for pub in publications:
        pub.selected = False

    # loop over selected files and select parameters and publications
    for filename in selected_files:
        (directory, name) = filename[:-4].split(os.path.sep, 1)
        if directory == 'parameters':
            if name.endswith('page'):
                for param in parameters.values():
                    if os.path.dirname(param.name) == os.path.dirname(name):
                        param.selected = True
            else:
                parameters[name].selected = True
        elif directory == 'measurements':
            for pub in publications:
                if pub.filename[:-4] == os.path.basename(name):
                    pub.selected = True

    # make sure all parameters of selected measurements are included
    for pub in publications:
        if pub.selected:
            for meas in pub.measurements:
                if meas.param in parameters.keys() and not parameters[meas.param].selected:
                    parameters[meas.param].selected = True


def create_averages(parameters):
    """Average the measurements of the given parameters and create a plot and a html page for each of them."""
    
    for param in list(parameters.keys()):
        if not parameters[param].selected:
            continue
        print('INFO: Averaging %s' % param)
        parameters[param].average()
        if parameters[param].data:
            parameters[param].plot()
            parameters[param].html()


def create_plot(outdir, index, html, params, plot_title, plot_unit):
    """Create an overview plot."""

    # create the plot axes
    n = len(params)
    width = 10
    height = (n+3) * 0.4
    dpi = 120
    fig = plt.figure(figsize=(width, height), dpi=dpi)
    if plot_title:
        title = plot_title
        if plot_unit != 1:
            title += r' $[10^{%d}]$' % math.log10(plot_unit)
        plt.title(title)
    plt.axis(ymin=0, ymax=n)
    plt.grid(True)
    transform = fig.get_axes()[0].transData

    # create the image map
    if html:
        html.write('<map name="overview%d">\n' % index)

    # plot each parameter
    y = 0.5
    yticks = []
    xzero = False
    for param in reversed(params):
        unit = get_value(1, param.data.unit) / plot_unit
        if param.data.is_limit():
            plot = plt.errorbar([param.data.value * unit], [y], xerr=[[param.data.value * unit], [0]], xuplims=True, capsize=6, mew=2, color='black')
            plot[1][0].set_marker(matplotlib.markers.CARETLEFT)
            plot[1][0].set_markeredgewidth(0)
            if xzero is False:
                xzero = True
        else:
            plot = plt.errorbar([param.data.value * unit], [y], xerr=[[abs(param.data.neg_total() * unit)], [param.data.pos_total() * unit]], fmt='o', capsize=0, color='black')
            plt.errorbar([param.data.value * unit], [y], xerr=[[abs(param.data.neg_stat() * unit)], [param.data.pos_stat() * unit]], capsize=6, mew=2, color='black')
            if len(param.data.errors) > 2:
                plt.errorbar([param.data.value * unit], [y], xerr=[[abs(param.data.neg_stat_syst() * unit)], [param.data.pos_stat_syst() * unit]], capsize=4, color='black')
            if param.data.value + param.data.neg_total() < 0:
                xzero = None

        # label
        yticks.append(r'$' + param.latex + '$')
        y += 1

    plt.yticks(0.5+np.arange(n), yticks)
    if xzero:
        plt.xlim(xmin=0)

    # adjust layout and create click map
    try:
        plt.tight_layout()
    except:
        pass
    hflav_logo(fig)
    if html:
        y = 0.5
        for param in reversed(params):
            yrange = transform.transform([(0, y-0.4), (0, y+0.4)])
            html.write('  <area shape="rect" coords="%d,%d,%d,%d" href="%s.html">\n' % (0, height*dpi-yrange[1][1], width*dpi, height*dpi-yrange[0][1], os.path.split(param.name)[-1]))
            y += 1

    # save figure and image map
    plt.savefig(os.path.join(outdir, 'overview%d.png' % index), format='png', dpi=dpi)
    plt.savefig(os.path.join(outdir, 'overview%d.pdf' % index), format='pdf', dpi=dpi)
    plt.title('')
    if plot_unit != 1:
        plt.xlabel(r'$10^{%d}$' % math.log10(plot_unit), fontsize=14)
        plt.subplots_adjust(bottom=0.27*4/(n+3))
    plt.savefig(os.path.join('latex', '_'.join(outdir.split(os.path.sep)[1:3]) + '_%d.pdf' % index), format='pdf', dpi=dpi)
    plt.subplots_adjust(bottom=None)
    plt.close()
    if html:
        html.write('</map>\n')
        html.write('<p><img src="overview%d.png" usemap="#overview%d"></p>\n' % (index, index))
        

def create_latex(outdir, index, params, caption, unit=None):
    """Create an overview latex table."""

    # create latex table
    latex = open(outdir + '_%d.tex' % index, 'w')
    latex.write('\\begin{btocharmtab}{%s}{%s' % (outdir.split(os.path.sep)[-1] + '_%d' % index, caption))
    latex.write('}\n')
    latex.write('\\hline\n')
    latex.write('\\textbf{Parameter} & \\begin{tabular}{l}\\textbf{Measurements}\\end{tabular}')
    if unit is not None and unit != 1:
        latex.write(' $[10^{%d}]$' % math.log10(unit))
    latex.write(' & \\textbf{Average}')
    if unit is not None and unit != 1:
        latex.write(' $[10^{%d}]$' % math.log10(unit))
    latex.write(' \\\\\n')
    latex.write('\\hline\n\\hline\n')

    # add a row for each parameter
    for param in params:
        latex.write(param.tex(unit))
        latex.write('\\hline\n')

    # add figure
    latex.write('\\end{btocharmtab}\n')
    if len(params) > 1:
        latex.write('\\btocharmfig{%s_%d}\n' % (os.path.basename(outdir), index))

    # close file
    latex.close()


def create_overview(subdir):
    """Create html page and plots for overview pages."""

    global parameters

    # get information from page.xml
    title = subdir
    plots = []
    if os.path.exists(os.path.join('parameters', subdir, 'page.xml')):
        try:
            tree = ET.parse(os.path.join('parameters', subdir, 'page.xml'))
        except Exception as exception:
            print('ERROR: The parsing of %s failed: %s' % (os.path.join('parameters', subdir, 'page.xml'), exception))
            return
        root = tree.getroot()
        title = child_text(root, 'title')
        plots = root.findall('plot')

    # write html header and title
    if not os.path.exists(os.path.join('html', subdir)):
        os.makedirs(os.path.join('html', subdir))
    html = open(os.path.join('html', subdir, 'index.html'), 'w')
    html.write("""<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>%s</title>
<script type="text/javascript"
  src="%s">
</script>
<link rel="stylesheet" type="text/css" href="../../style.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
    $(function(){
      $("#nav-placeholder").load("../../navbar.html");
    });
</script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<meta name="theme-color" content="#fafafa">
</head>
<body>
<div id="nav-placeholder"></div>
<h2>%s</h2>
""" % (title, mathjax_url, title))

    # create plots
    files = glob.glob(os.path.join('html', subdir, '*.html'))
    index = 1
    for plot in plots:
        plot_title = child_text(plot, 'title', title)
        plot_unit = child_float(plot, 'unit', 1)
        params = []
        for param in plot.findall('param'):
            name = os.path.join(subdir, param.text)
            if not parameters[name].selected:
                continue
            html_name = os.path.join('html', name + '.html')
            if html_name not in files:
                print('ERROR: Plot of unknown parameter (%s) requested in %s.' % (name, os.path.join('parameters', subdir, 'page.xml')))
                continue
            files.remove(html_name)
            if parameters[name].data:
                params.append(parameters[name])
        if len(params) == 0:
            continue
        print('INFO: Creating %s plot overview %s' % (subdir, plot_title))
        create_plot(os.path.join('html', subdir), index, html, params, plot_title, plot_unit)
        create_latex(os.path.join('latex', subdir.replace(os.path.sep, '_')), index, params, plot_title, plot_unit)
        index += 1

    # list remaining parameters
    params = []
    for filename in sorted(files):
        if filename.endswith('index.html'):
            continue
        print('WARNING: The parameter %s is not included in any overview plot.' % filename[5:-5])
        param = parameters[filename[5:-5]]
        if param.data:
            params.append(param)
        html.write('<p><a href="%s">\\(%s\\)</p>\n' % (os.path.split(filename)[-1], param.latex))
    if params:
        create_latex(os.path.join('latex', subdir.replace(os.path.sep, '_')), 0, params, 'Remaining parameters')

    # close html file
    html.write('</body>\n</html>\n')
    html.close()


def create_webpages(topdir):
    """Traverse the given directory and its subdirectories and create the index html page."""
    
    for html in glob.glob('*.html') + glob.glob('*.css'):
        shutil.copyfile(html, os.path.join('html', html))

    if not os.path.exists('latex'):
        os.makedirs('latex')

    for (dirname, dirs, files) in os.walk(topdir):
        if len([xml for xml in files if xml.endswith('.xml')]) > 0:
            subdir = os.path.sep.join(dirname.split(os.path.sep)[1:])
            create_overview(subdir)


def create_pubpages():
    """Create the html pages for the publication information."""

    global publications
    for pub in publications:
        if pub.selected:
            pub.html()

    global experiment_colors
    for experiment in list(experiment_colors.keys()):
        pubs = glob.glob(os.path.join('html', 'publications', experiment, '*.html'))
        if len(pubs) == 0:
            continue
        html = open(os.path.join('html', 'publications', experiment, 'index.html'), 'w')
        html.write("""<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>%s</title>
<link rel="stylesheet" type="text/css" href="../../style.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
    $(function(){
      $("#nav-placeholder").load("../../navbar.html");
    });
</script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<meta name="theme-color" content="#fafafa">
</head>
<body>
<div id="nav-placeholder"></div>
<h2>%s Publications</h2>
<ul>
""" % (experiment, experiment))
        for pub in sorted(pubs):
            if pub.endswith('index.html'):
                continue
            basename = os.path.splitext(os.path.split(pub)[-1])[0]
            html.write('<li><a href="%s.html">%s</a></li>\n' % (basename, basename))
        html.write('</body>\n</html>\n')
        html.close()


def create_menu():
    """Create the menu page"""

    html = open(os.path.join('html', 'menu.html'), 'w')
    html.write("""<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>HFLAV: B to charm</title>
<link rel="stylesheet" type="text/css" href="style.css">
<style type="text/css">
h2,h3 { margin-bottom: -10px; }
ul { padding-left: 20px; }
</style>
</head>
<body>
<h2>B to charm</h2>
""")

    bcaption = {'Bd': 'B<sup>0</sup> decays to',
                'Bu': 'B<sup>-</sup> decays to',
                'B': 'B<sup>0/-</sup> decays to',
                'Bs': 'B<sub>s</sub> decays to',
                'Bc': 'B<sub>c</sub> decays to',
                'Bbaryon': 'B baryon decays to'
                }
    ccaption = {'D': 'Single open charm mesons',
                'DD': 'Double open charm mesons',
                'cc': 'Charmonium',
                'baryon': 'Charm baryons',
                'other': 'Other states',
                'B': 'B mesons'}

    for bhadron in ['Bd', 'Bu', 'B', 'Bs', 'Bc', 'Bbaryon']:
        if len(glob.glob(os.path.join('html', bhadron, '*', '*.png'))) > 0:
            html.write('\n<h3>%s</h3>\n<ul>\n' % bcaption[bhadron])
            for chadron in ['D', 'DD', 'cc', 'baryon', 'other', 'B']:
                if len(glob.glob(os.path.join('html', bhadron, chadron, '*.png'))) > 0:
                    html.write('<li><a target="data" href="%s/%s/index.html">%s</a></li>\n' % (bhadron, chadron, ccaption[chadron]))
            html.write('</ul>\n')

    html.write('\n<h3>Publications:</h3>\n<ul>\n')
    for experiment in sorted(list(experiment_colors.keys())):
        if os.path.exists(os.path.join('html', 'publications', experiment, 'index.html')):
            html.write('<li><a target="data" href="publications/%s/index.html">%s</a></li>\n' % (experiment, experiment))
    html.write('</ul>\n')

    html.write('</body>\n</html>\n')
    html.close()


def create_document():
    """Create a latex document that includes all overview tables."""
    
    latex = open(os.path.join('latex', 'b2charm.tex'), 'w')
    latex.write(r"""\documentclass[12pt,twoside]{article}
\usepackage{morefloats}
\usepackage{mciteplus}
\usepackage{hyperref}
\usepackage{longtable}
\usepackage{float}
\usepackage[pdftex]{graphicx}
\usepackage[math]{cellspace}
\cellspacetoplimit 2pt
\cellspacebottomlimit 2pt
\usepackage{wasysym}

\setlength{\oddsidemargin}{-0.4cm}
\setlength{\evensidemargin}{-0.4cm}
\setlength{\textwidth}{17cm}  %<- {16cm}
\setlength{\textheight}{23.5cm}
\setlength{\topmargin}{-2.0cm}

\def\babar{BaBar}
\def\dzero{D0}

\newenvironment{btocharmtab}[2]{\begin{table}[H]\begin{center}\caption{#2.}\label{tab:b2c:#1}\begin{tabular}{| Sl l l |}}{\end{tabular}\end{center}\end{table}}
\newcommand{\btocharmfig}[1]{\begin{figure}[H]\begin{center}\includegraphics[width=0.99\textwidth]{#1}\caption{Summary of the averages from Table~\ref{tab:b2c:#1}.}\label{fig:b2c:#1}\end{center}\end{figure}}
\newcommand{\btocharmparam}[3]{#1#3}
\newcommand{\btocharmparaml}[3]{\multicolumn{3}{|Sl|}{#2#3}\\}
\newcommand{\btocharm}[1]{\input{#1}}

\begin{document}
""")
    
    bcaption = {'Bd': '$\\bar{B}^0$',
                'Bu': '$B^-$',
                'B': '$\\bar{B}^0$ / $B^-$',
                'Bs': '$\\bar{B}_s^0$',
                'Bc': '$B_c^-$',
                'Bbaryon': '$b$ baryons'}
    ccaption = {'D': 'Decays to a single open charm meson',
                'DD': 'Decays to two open charm mesons',
                'cc': 'Decays to charmonium states',
                'baryon': 'Decays to charm baryons',
                'other': 'Decays to $XYZP$ states',
                'B': 'Decays to a $B$ meson'}
    for bhadron in ['Bd', 'Bu', 'B', 'Bs', 'Bc', 'Bbaryon']:
        hadrons = 'baryons' if bhadron == 'Bbaryon' else 'mesons'
        latex.write('\n\\subsection{Decays of %s %s}\n\\label{sec:b2c:%s}\n' % (bcaption[bhadron], hadrons, bhadron))
        subsubsections = [chadron for chadron in ['D', 'DD', 'cc', 'baryon', 'other', 'B'] if os.path.exists(os.path.join('latex', bhadron + '_' + chadron + '_0.tex')) or os.path.exists(os.path.join('latex', bhadron + '_' + chadron + '_1.tex')) ]
        if len(subsubsections) == 0:
            continue
        latex.write('Measurements of %s decays to charmed hadrons are summarized in ' % (bcaption[bhadron]))
        if len(subsubsections) == 1:
            latex.write('Section~\\ref{sec:b2c:%s_%s}.\n' % (bhadron, subsubsections[0]))
        else:
            latex.write('Sections~\\ref{sec:b2c:%s_%s} to~\\ref{sec:b2c:%s_%s}.\n' % (bhadron, subsubsections[0], bhadron, subsubsections[-1]))

        for chadron in subsubsections:
            latex.write('\n\\subsubsection{%s}\n\\label{sec:b2c:%s_%s}\n' % (ccaption[chadron], bhadron, chadron))
            tables = ['%s_%s_%d' % (bhadron, chadron, index) for index in list(range(1,100)) + [0] if os.path.exists(os.path.join('latex', '%s_%s_%d.tex' % (bhadron, chadron, index)))]
            s = 's' if len(tables) > 1 else ''
            latex.write('Averages of %s d%s are shown in Table%s~\\ref{tab:b2c:%s}' % (bcaption[bhadron], ccaption[chadron][1:], s, tables[0]))
            if len(tables) > 1:
                latex.write('--\\ref{tab:b2c:%s}' % tables[-1])
            latex.write(' and Fig%s.~\\ref{fig:b2c:%s}' % (s, tables[0]))
            if len(tables) > 1:
                latex.write('--\\ref{fig:b2c:%s}' % tables[-1])
            latex.write('.\n')

            for table in tables:
                latex.write('\\btocharm{%s}\n' % table)

    latex.write('\n\\bibliographystyle{../HFLAV}\n')
    latex.write('\\raggedright\n')
    latex.write('\\bibliography{b2charm}\n')

    latex.write('\n\\end{document}\n')
    latex.close()


def average():
    from optparse import OptionParser
    usage = """Usage: python3 -m averaging [options] [arguments]

    Arguments can be used to select parameters and measurements for the averaging. File or directory names can be specified."""
    parser = OptionParser(usage)
    parser.add_option('-d', '--debug', action='store_true', dest='debug', default=False, help='include debug information in web pages')
    parser.add_option('-s', '--superseded', dest='show_superseded', default='web', help='where to show superseded results: none, web, tables, all')
    parser.add_option('-x', '--exclude', action='append', type='string', dest='exclude', help='bibtex file whose entries will not be added to b2charm.bib')
    parser.add_option('-g', '--git', action='store', type='string', dest='git', help='git reference, parameters and measurements which changed with respect to this reference are selected')
    parser.add_option('-t', '--title', action='store', type='string', dest='title', help='sub-title in logo')
    (options, arguments) = parser.parse_args()
    debug = options.debug
    show_superseded = options.show_superseded
    if options.title:
        subtitle = options.title.replace('_', ' ')

    read_parameters('parameters', parameters)
    read_parameters('inputs', inputs)
    read_measurements('measurements', publications)
    apply_selection(arguments, options.git, parameters, publications)
    create_averages(parameters)
    create_webpages('parameters')
    create_pubpages()
    create_menu()
    create_document()

    excluded = []
    bibentryreg = re.compile('^\s*@\w*{(\S*),', re.M)
    if options.exclude:
        for excludefile in options.exclude:
            excluded = excluded + bibentryreg.findall(open(excludefile).read())
    bibtexfile = open(os.path.join('latex', 'b2charm.bib'), 'w')
    for (key, entry) in list(bibtex.items()):
        if key in excluded:
            continue
        try:
            bibtexfile.write(entry.replace('&gt;', '>'))
        except Exception as exception:
            print('ERROR: The following entry could not be written to the bibtex file. Check for non-ascii character.', exception)
            print(entry)
    bibtexfile.close()
