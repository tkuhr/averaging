#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re


def child_text(xml, tag, default = None):
    """Get the data of the given tag sub-element of an xml element.
    If the tag element does not exists a default value is returned"""
    
    if xml is None:
        return default
    child = xml.find(tag)
    if child is None:
        return default
    return child.text


def child_float(xml, tag, default = None):
    """Get the data of the given tag sub-element of an xml element converted to a float.
    If the tag element does not exists a default value is returned"""
    
    if xml is None:
        return default
    child = xml.find(tag)
    if child is None:
        return default
    return float(child.text)


def get_value(default, *args):
    """Return the first argueent that is not None or False.
    If there is no such argument the default value is returned"""
    
    for arg in args:
        if arg:
            return arg
    return default


def find_reg(data, regexp):
    """Helper function to return the first match of a regular expression or None."""
    
    reg = re.compile(regexp, re.M)
    record = reg.findall(data)
    if len(record) == 0:
        return None
    else:
        return record[0]
