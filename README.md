# Averaging tool of the HFLAV b2charm and rare decays groups

## Installation

```bash
python3 setup.py install --user
```
## Execution

In a data directory (with measurements and parameters directories) run the `hflav_average` script.

